# YACO

_YACO_ (_Yet Another Contig Ordering_) is a command line program written in [Bash](https://www.gnu.org/software/bash/) for orienting and ordering contigs (generally, a draft genome assembly) according to a close(d) reference genome. Based on reciprocal BLAST searches, _YACO_ is a simple and practical tool, which generally achieves accurate results with acceptable running times (e.g. less than 10 seconds to process a 5 Mbp draft genome using 12 threads).

_YACO_ runs on UNIX, Linux and most OS X operating systems.


## Dependencies

You will need to install the required programs listed in the following table, or to verify that they are already installed with the required version.

<div align="center">

| program                                      | package                                                 | version     | sources                                                                                                  |
|:------------------------------------------------------ |:-------------------------------------------------------:| -----------:|:--------------------------------------------------------------------------------------------------------- |
| [_gawk_](https://www.gnu.org/software/gawk/) | -                                                       | > 4.0.0     | [ftp.gnu.org/gnu/gawk](http://ftp.gnu.org/gnu/gawk/)                                                     |
| _makeblastdb_ <br> _blastn_                  | [blast+](https://www.ncbi.nlm.nih.gov/books/NBK279690/) | &ge; 2.12.0 | [ftp.ncbi.nlm.nih.gov/blast/executables/blast+](https://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/)  |

</div>


## Installation and execution

Clone this repository with the following command line:

```bash
git clone https://gitlab.pasteur.fr/GIPhy/YACO.git
```

Go to the directory `GenoLayout/` to give the execute permission to the file: 

```bash
cd YACO/
chmod +x YACO.sh
```
and run it with the following command line model:

```bash
./YACO.sh [options]
```

If at least one of the indicated programs (see [Dependencies](#dependencies)) is not available on your `$PATH` variable (or if one compiled binary has a different default name), _YACO_ will exit with an error message (when the requisite programs are missing).
To set a required program that is not available on your `$PATH` variable, edit the file and indicate the local path to the corresponding binary(ies) within the code block `REQUIREMENTS`.


## Usage

Run _YACO_ without option to read the following documentation:

```
 USAGE:  YACO  -i <infile>  -r <reffile>  -o <basename>  [options]

 OPTIONS:
  -i <file>    FASTA file containing contigs to orient/order (mandatory)
  -r <file>    FASTA file containing the reference sequence (mandatory)
  -o <string>  basename for output files (mandatory)
  -w <int>     window (fragment) size (default: 400)
  -p <real>    minimum proportion of ranked fragments per contig (default: 0.5)
  -k <int>     blastn word size (default: 15)
  -c           contig cutting to fit circular reference genome (default: not set)
  -l <int>     wrap output sequences to the specified width (default: infinity)
  -t <int>     number of threads (default: 2)
  -v           verbose mode
  -h           prints this help and exits
```

## Notes

* First, given a fixed fragment length _w_ (option `-w`; default: 400 bps), _YACO_ partitions each contig (option `-i`) into consecutive fragments _f<sub>i</sub>_, and decomposes the reference sequence(s) (option `-r`) into overlapping fragments _f<sub>r</sub>_ (step _w_&nbsp;&#8725;&nbsp;2). Next, each set of fragments is searched against the other using _blastn_ (Altschul et al. 1990; Camacho et al. 2008) with tuned parameters (as suggested by Goris et al. 2007). Orthologous fragments are assessed by reciprocal best BLAST hits showing &geq;&nbsp;30 % overall fragment identity on an alignable region &geq;&nbsp;35% fragment length (as suggested by Lee et al. 2016). Every fragment _f<sub>i</sub>_ associated with an orthologous one _f<sub>r</sub>_ is ranked by the position of _f<sub>r</sub>_ within the reference. A contig is localized when a sufficient proportion of its fragments _f<sub>i</sub>_ is ranked (as set by option `-p`, default: &geq;50%). Every localized contig is replaced by its reverse-complement when most of its ranked fragments _f<sub>i</sub>_ are in opposite strand against their orthologous fragments _f<sub>r</sub>_. Finally, the localized contigs are sorted according to the median rank of its fragments _f<sub>i</sub>_. Oriented and ordered contigs are finally written into the specified output file. 

* When the reference file (option `-r`) contains more than one sequence, (reciprocal) BLAST searches are performed against all of them, but the ordering/orienting procedure (see above)  is carried out according to only the first one. This approach can be useful to make a better distinction between a reference chromosome and e.g. several reference plasmids.

* When the (first) reference sequence is expected to be a circular genome, the option `-c` can be used to cut the contig that is orthologous to both extremities of the reference  (if any).

* Faster running times can be obtained in three ways: (i) by using large BLAST _k_-mer lengths (option `-k`; default: 15), but at the cost of a reduced accuracy; (ii) by using larger fragment lengths (option `-w`; default: 400), but at the cost of a reduced number of orthologous fragment pairs; (iii) by using a large number of threads (option `-t`). 

* For distantly-related genomes (e.g. expected average nucleotide identity &leq;&nbsp;80%), it is recommended to use short _k_-mers for BLAST searches (e.g. `-k 11`).

* Each input file (options `-i` and `-r`) should be in FASTA format, not compressed, and may contain nucleotide sequences. 

* Output file prefix name should be set using option `-o`. Localized contigs are ordered, oriented and written in FASTA format into the file with extension `.reo.fasta`. Unlocalized contigs are written in FASTA format into the file with extension `.unl.fasta`. A summary is written in tab-delimited format into the file with extension `.info.txt` (i.e. contig name, orientation, and proportion of ranked fragments).



## Examples

The directory _figures/_ contains different SVG files (created using [_GenoLayout_](https://gitlab.pasteur.fr/GIPhy/GenoLayout)) that illustrate the ability of _YACO_ to orient and order different _Klebsiella pneumoniae_ draft genomes.


##### Ordering/orienting contigs according to a reference chromosome

The draft genome of _K. pneumoniae_ SB1139 (45 contigs, 5.34 Mbp) can be downloaded using the following command lines:

```bash
FTPGCA="https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA";
wget -q -O - $FTPGCA/900/977/655/GCA_900977655.1_SB1139.1/GCA_900977655.1_SB1139.1_genomic.fna.gz | zcat > Kp.SB1139.fa ;
```

The reference chromosome of _K. pneumoniae_ SB612 can be downloaded using the following command lines:

```bash
EUTILS="https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nuccore&rettype=fasta&id=";
wget -q -O Kp.SB612.chr.fa $EUTILS""CP084830 ;
```

_YACO_ can orient/order the SB1139 contigs according to the SB612 chromosome using the following command line:

```bash
YACO.sh  -t 12  -i Kp.SB1139.fa  -r Kp.SB612.chr.fa  -o Kp.SB1139
``` 

The output file _Kp.SB1139.into.txt_ (reproduced below) shows that _YACO_ is able to localize 36 contigs: 


<pre style="font-size: 0.3em">#ordered/oriented sequences
CAAHFT010000037.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_42, whole genome shotgun sequence - 2/2
CAAHFT010000017.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_24, whole genome shotgun sequence - 664/667
CAAHFT010000016.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_23, whole genome shotgun sequence - 171/174
CAAHFT010000015.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_22, whole genome shotgun sequence - 91/91
CAAHFT010000014.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_21, whole genome shotgun sequence - 66/66
CAAHFT010000013.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_20, whole genome shotgun sequence - 305/357
CAAHFT010000011.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_19, whole genome shotgun sequence - 573/828
CAAHFT010000010.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_18, whole genome shotgun sequence - 846/906
CAAHFT010000040.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_45, whole genome shotgun sequence - 3/3
CAAHFT010000024.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_30, whole genome shotgun sequence + 5/5
CAAHFT010000035.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_40, whole genome shotgun sequence - 1/1
CAAHFT010000033.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_39, whole genome shotgun sequence - 3/3
CAAHFT010000036.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_41, whole genome shotgun sequence + 1/2
CAAHFT010000027.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_33, whole genome shotgun sequence - 2/4
CAAHFT010000009.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_17, whole genome shotgun sequence - 1701/1938
CAAHFT010000008.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_16, whole genome shotgun sequence - 698/801
CAAHFT010000007.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_15, whole genome shotgun sequence - 1116/1171
CAAHFT010000006.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_14, whole genome shotgun sequence - 317/404
CAAHFT010000005.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_13, whole genome shotgun sequence - 120/144
CAAHFT010000004.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_12, whole genome shotgun sequence - 1339/1383
CAAHFT010000003.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_11, whole genome shotgun sequence - 388/388
CAAHFT010000002.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_10, whole genome shotgun sequence - 1062/1195
CAAHFT010000029.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_35, whole genome shotgun sequence - 4/4
CAAHFT010000025.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_31, whole genome shotgun sequence - 7/7
CAAHFT010000045.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_9, whole genome shotgun sequence  - 239/239
CAAHFT010000038.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_43, whole genome shotgun sequence + 1/2
CAAHFT010000044.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_8, whole genome shotgun sequence  - 633/634
CAAHFT010000043.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_7, whole genome shotgun sequence  - 51/53
CAAHFT010000042.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_6, whole genome shotgun sequence  - 520/611
CAAHFT010000041.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_5, whole genome shotgun sequence  - 309/309
CAAHFT010000034.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_4, whole genome shotgun sequence  - 88/88
CAAHFT010000023.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_3, whole genome shotgun sequence  - 12/12
CAAHFT010000022.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_29, whole genome shotgun sequence - 1/1
CAAHFT010000012.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_2, whole genome shotgun sequence  - 217/217
CAAHFT010000001.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_1, whole genome shotgun sequence  - 259/260
CAAHFT010000018.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_25, whole genome shotgun sequence - 252/253
#unlocalized sequences
CAAHFT010000019.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_26, whole genome shotgun sequence
CAAHFT010000020.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_27, whole genome shotgun sequence
CAAHFT010000021.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_28, whole genome shotgun sequence
CAAHFT010000026.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_32, whole genome shotgun sequence
CAAHFT010000028.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_34, whole genome shotgun sequence
CAAHFT010000030.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_36, whole genome shotgun sequence
CAAHFT010000031.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_37, whole genome shotgun sequence
CAAHFT010000032.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_38, whole genome shotgun sequence
CAAHFT010000039.1 Klebsiella pneumoniae isolate SB1139 genome assembly, contig: SB1139_Kp1_44, whole genome shotgun sequence
</pre>

The figure below represents the linear map of the initial contig order (**Kp.SB1139**, up) according to the reference chromosome (**Kp.SB612.chr**, middle), as well as those of the 36 ordered contigs (output file _Kp.SB1139.reo.fasta_) returned by _YACO_ (**Kp.SB1139.reo**, bottom).

<!--- GenoLayout.sh -t 95 -w 800 -k 11 -d 5 -a yellow -o Kp.SB1139.svg    Kp.SB1139.fa Kp.SB612.chr.fa Kp.SB1139.reo.fasta -->

<p align="center">
  <img  align="center" src="figures/Kp.SB1139.svg">
</p>

This figure shows that the 36 contigs ordered/oriented by _YACO_ (**Kp.SB1139.reo**, bottom) are broadly colinear (as expected) with the reference chromosome (**Kp.SB612.chr**, middle). It also shows that the last contig (CAAHFT010000018, see _Kp.SB1139.into.txt_ above) is orthologous with both extremities of the circular SB612 chromosome.

To improve the overall colinearity, _YACO_ can be run using option `-c`: 

```bash
YACO.sh  -t 12  -i Kp.SB1139.fa  -r Kp.SB612.chr.fa  -o Kp.SB1139.cut  -c
``` 

This option enables to cut the contig CAAHFT010000018 (at position 50747; not shown) prior to the ordering procedure. The cutting/ordering/orienting carried out by _YACO_ is illustrated below: 

<!--- GenoLayout.sh -t 95 -w 800 -k 11 -d 5 -a yellow -o Kp.SB1139.cut.svg    Kp.SB612.chr.fa Kp.SB1139.cut.reo.fasta -->

<p align="center">
  <img  align="center" src="figures/Kp.SB1139.cut.svg">
</p>

<br>

##### Ordering/orienting contigs according to a multiple sequence reference (chromosome + plasmids)

The draft genome of _K. pneumoniae_ SB612 (418 contigs, 5.73 Mbp) can be downloaded (file _SB612.fa_) using the following command lines:

```bash
FTPGCA="https://ftp.ncbi.nlm.nih.gov/genomes/all/GCA";
wget -q -O - $FTPGCA/900/978/345/GCA_900978345.1_2-3.1/GCA_900978345.1_2-3.1_genomic.fna.gz | zcat > SB612.fa ;
```

The complete genome of the same strain (one chromosome and 12 plasmid sequences) can be downloaded (files _chr.fa_ and _p01.fa_-_p12.fa_, respectively) using the following command lines:

```bash
EUTILS="https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nuccore&rettype=fasta&id=";
wget -q -O chr.fa $EUTILS""CP084830 ;
wget -q -O p01.fa $EUTILS""CP084831 ;
wget -q -O p02.fa $EUTILS""CP084832 ;
wget -q -O p03.fa $EUTILS""CP084833 ;
wget -q -O p04.fa $EUTILS""CP084834 ;
wget -q -O p05.fa $EUTILS""CP084835 ;
wget -q -O p06.fa $EUTILS""CP084836 ;
wget -q -O p07.fa $EUTILS""CP084837 ;
wget -q -O p08.fa $EUTILS""CP084838 ;
wget -q -O p09.fa $EUTILS""CP084839 ;
wget -q -O p10.fa $EUTILS""CP084840 ;
wget -q -O p11.fa $EUTILS""CP084841 ;
wget -q -O p12.fa $EUTILS""CP084842 ;
```

The 13 replicons can be pooled into a unique file (_all.fa_) using the following command line: 

```bash
cat chr.fa p01.fa p02.fa p03.fa p04.fa p05.fa p06.fa p07.fa p08.fa p09.fa p10.fa p11.fa p12.fa > all.fa
```

To orient/order the 418 contigs according to the SB612 complete chromosome using _YACO_, run the following command line:

```bash
YACO.sh  -t 12  -i SB612.fa  -r all.fa  -o SB612.chr
``` 

This enables to localize 315 contigs (output file _SB612.chr.reo.fasta_; not shown) according to the SB612 complete chromosome.
The figure below represents the linear maps between the 418 contigs (**SB612**, up), the 13 replicons (**all**), the sole reference chromosome (**chr**), and the 315 ordered/oriented contigs (**SB612.chr.reo**, bottom).

<!--- GenoLayout.sh -t 95 -w 950 -d 3 -o SB612.chr.svg  SB612.fa all.fa chr.fa SB612.chr.reo.fasta -->

<p align="center">
  <img  align="center" src="figures/SB612.chr.svg">
</p>

This figure shows that almost all the reference chromosome (**chr**) is covered by the 315 contigs localized by _YACO_ (**SB612.chr.reo**). It also shows that the unlocalized contigs are likely plasmid sequences, as the 12 reference plasmids (right part of **all**) seem associated to a subset of the initial contigs (**SB612**).

The 12 plasmid sequences can be pooled into a unique file (_pla.fa_) using the following command line: 

```bash
cat p01.fa p02.fa p03.fa p04.fa p05.fa p06.fa p07.fa p08.fa p09.fa p10.fa p11.fa p12.fa > p01-12.fa
```

To orient/order the 103 unlocalized contigs (output file _SB612.chr.unl.fasta_) according to the first SB612 plasmid (first sequence in file _pla01-12.fa_) using _YACO_, run the following command line:

```bash
YACO.sh  -t 12  -i SB612.chr.unl.fasta  -r p01-12.fa  -o SB612.p01
```

This enables to localize 31 contigs (output file _SB612.p01.reo.fasta_; not shown) according to the first plasmid sequence (file _p01.fa_).
The figure below represents the linear maps between the 103 non-chromosome contigs (**SB612.chr.unl**, up), the 12 plasmid sequences (**p01-12**), the first plasmid (**p01**), and the 31 ordered/oriented contigs (**SB612.p01.reo**, bottom).

<!--- GenoLayout.sh -t 95 -w 100 -d 3 -o SB612.p01.svg SB612.chr.unl.fasta  p01-12.fa p01.fa  SB612.p01.reo.fasta -->

<p align="center">
  <img  align="center" src="figures/SB612.p01.svg">
</p>

The same approach can be reiterated to localize the 72 remaining contigs (file _SB612.p01.unl.fasta_) on the 11 last plasmids (files _p02.fa_-_p12.fa_), e.g.

```bash
cat p02.fa p03.fa p04.fa p05.fa p06.fa p07.fa p08.fa p09.fa p10.fa p11.fa p12.fa > p02-12.fa
YACO.sh  -t 12  -i SB612.p01.unl.fasta  -r p02-12.fa  -o SB612.p02

cat p03.fa p04.fa p05.fa p06.fa p07.fa p08.fa p09.fa p10.fa p11.fa p12.fa > p03-12.fa
YACO.sh  -t 12  -i SB612.p02.unl.fasta  -r p03-12.fa  -o SB612.p03

cat p04.fa p05.fa p06.fa p07.fa p08.fa p09.fa p10.fa p11.fa p12.fa > p04-12.fa
YACO.sh  -t 12  -i SB612.p03.unl.fasta  -r p04-12.fa  -o SB612.p04

cat p05.fa p06.fa p07.fa p08.fa p09.fa p10.fa p11.fa p12.fa > p05-12.fa
YACO.sh  -t 12  -i SB612.p04.unl.fasta  -r p05-12.fa  -o SB612.p05

cat p06.fa p07.fa p08.fa p09.fa p10.fa p11.fa p12.fa > p06-12.fa
YACO.sh  -t 12  -i SB612.p05.unl.fasta  -r p06-12.fa  -o SB612.p06

cat p07.fa p08.fa p09.fa p10.fa p11.fa p12.fa > p07-12.fa
YACO.sh  -t 12  -i SB612.p06.unl.fasta  -r p07-12.fa  -o SB612.p07

cat p08.fa p09.fa p10.fa p11.fa p12.fa > p08-12.fa
YACO.sh  -t 12  -i SB612.p07.unl.fasta  -r p08-12.fa  -o SB612.p08

cat p09.fa p10.fa p11.fa p12.fa > p09-12.fa
YACO.sh  -t 12  -i SB612.p08.unl.fasta  -r p09-12.fa  -o SB612.p09

cat p10.fa p11.fa p12.fa > p10-12.fa
YACO.sh  -t 12  -i SB612.p09.unl.fasta  -r p10-12.fa  -o SB612.p10

cat p11.fa p12.fa > p11-12.fa
YACO.sh  -t 12  -i SB612.p10.unl.fasta  -r p11-12.fa  -o SB612.p11

YACO.sh  -t 12  -i SB612.p11.unl.fasta  -r p12.fa     -o SB612.p12
```


Figures below illustrate the different localized contigs at each step.

<!--- GenoLayout.sh -t 95 -w 100 -d 3 -o SB612.p02.svg  SB612.p01.unl.fasta p02-12.fa p02.fa SB612.p02.reo.fasta -->
<!--- GenoLayout.sh -t 95 -w 100 -d 3 -o SB612.p03.svg  SB612.p02.unl.fasta p03-12.fa p03.fa SB612.p03.reo.fasta -->
<!--- GenoLayout.sh -t 95 -w 100 -d 3 -o SB612.p04.svg  SB612.p03.unl.fasta p04-12.fa p04.fa SB612.p04.reo.fasta -->
<!--- GenoLayout.sh -t 95 -w 100 -d 3 -o SB612.p05.svg  SB612.p04.unl.fasta p05-12.fa p05.fa SB612.p05.reo.fasta -->
<!--- GenoLayout.sh -t 95 -w 100 -d 3 -o SB612.p06.svg  SB612.p05.unl.fasta p06-12.fa p06.fa SB612.p06.reo.fasta -->
<!--- GenoLayout.sh -t 95 -w 115 -d 3 -o SB612.p07.svg  SB612.p06.unl.fasta p07-12.fa p07.fa SB612.p07.reo.fasta -->
<!--- GenoLayout.sh -t 95 -w 115 -d 3 -o SB612.p08.svg  SB612.p07.unl.fasta p08-12.fa p08.fa SB612.p08.reo.fasta -->
<!--- GenoLayout.sh -t 95 -w 125 -d 3 -o SB612.p09.svg  SB612.p08.unl.fasta p09-12.fa p09.fa SB612.p09.reo.fasta -->
<!--- GenoLayout.sh -t 95 -w 125 -d 3 -o SB612.p10.svg  SB612.p09.unl.fasta p10-12.fa p10.fa SB612.p10.reo.fasta -->
<!--- GenoLayout.sh -t 95 -w 100 -d 3 -o SB612.p11.svg  SB612.p10.unl.fasta p11-12.fa p11.fa SB612.p11.reo.fasta -->
<!--- GenoLayout.sh -t 95 -w 100 -d 3 -o SB612.p12.svg  SB612.p11.unl.fasta p12.fa SB612.p12.reo.fasta -->

<table border="0" cellspacing="0" cellpadding="0">
 <tr>
  <td width="33%"><b>p02</b><img  align="center" width="100%" src="figures/SB612.p02.svg"></td>
  <td width="33%"><b>p03</b><img  align="center" width="100%" src="figures/SB612.p03.svg"></td>
  <td width="33%"><b>p04</b><img  align="center" width="100%" src="figures/SB612.p04.svg"></td>
 </tr>
 <tr>
 </tr>
 <tr>
  <td width="33%"><b>p05</b><img  align="center" width="100%" src="figures/SB612.p05.svg"></td>
  <td width="33%"><b>p06</b><img  align="center" width="100%" src="figures/SB612.p06.svg"></td>
  <td width="33%"><b>p07</b><img  align="center" width="100%" src="figures/SB612.p07.svg"></td>
 </tr>
 <tr>
 </tr>
 <tr>
  <td width="33%"><b>p08</b><img  align="center" width="100%" src="figures/SB612.p08.svg"></td>
  <td width="33%"><b>p09</b><img  align="center" width="100%" src="figures/SB612.p09.svg"></td>
  <td width="33%"><b>p10</b><img  align="center" width="100%" src="figures/SB612.p10.svg"></td>
 </tr>
 <tr>
 </tr>
 <tr>
  <td width="33%"><b>p11</b><img  align="center" width="100%" src="figures/SB612.p11.svg"></td>
  <td width="33%"><b>p12</b><img  align="center" width="100%" src="figures/SB612.p12.svg"></td>
  <td width="33%"> </td>
 </tr>
</table>






## References

Altschul SF, Gish W, Miller W, Myers EW, Lipman DJ (1990) _Basic local alignment search tool_. **Journal of Molecular Biology**, 215(3):403-410. [doi:10.1016/S0022-2836(05)80360-2](https://www.sciencedirect.com/science/article/pii/S0022283605803602)

Camacho C, Coulouris G, Avagyan V, Ma N, Papadopoulos J, Bealer K, Madden TL (2008) _BLAST+: architecture and applications_. **BMC Bioinformatics**, 10:421. [doi:10.1186/1471-2105-10-421](https://doi.org/10.1186/1471-2105-10-421)

Goris J, Konstantinidis KT, Klappenbach JA, Coenye T, Vandamme P, Tiedje JM (2007) _DNA-DNA hybridization values and their relationship to whole-genome sequence similarities_. **International Journal of Systematic and Evolutionary Biology**, 57(1):81-91. [doi:10.1099/ijs.0.64483-0](https://doi.org/10.1099/ijs.0.64483-0)

Lee I, Kim YO, Park S-C, Chun J (2016) _OrthoANI: An improved algorithm and software for calculating average nucleotide identity_. **International Journal of Systematic and Evolutionary Biology**, 66(2):1100-1103. [doi:10.1099/ijsem.0.000760](https://doi.org/10.1099/ijsem.0.000760)


