#!/bin/bash

##############################################################################################################
#                                                                                                            #
#  YACO: Yet Another Contig Ordering                                                                         #
#                                                                                                            #
   COPYRIGHT="Copyright (C) 2022 Institut Pasteur"                                                           #
#                                                                                                            #
#  This program  is free software:  you can  redistribute it  and/or modify it  under the terms  of the GNU  #
#  General Public License as published by the Free Software Foundation, either version 3 of the License, or  #
#  (at your option) any later version.                                                                       #
#                                                                                                            #
#  This program is distributed in the hope that it will be useful,  but WITHOUT ANY WARRANTY;  without even  #
#  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public  #
#  License for more details.                                                                                 #
#                                                                                                            #
#  You should have received a copy of the  GNU General Public License along with this program.  If not, see  #
#  <http://www.gnu.org/licenses/>.                                                                           #
#                                                                                                            #
#  Contact:                                                                                                  #
#   Alexis Criscuolo                                                            alexis.criscuolo@pasteur.fr  #
#   Genome Informatics & Phylogenetics (GIPhy)                                             giphy.pasteur.fr  #
#   Bioinformatics and Biostatistics Hub                              research.pasteur.fr/en/team/hub-giphy  #
#   Dpt. Biologie Computationnelle            research.pasteur.fr/team/bioinformatics-and-biostatistics-hub  #
#   Institut Pasteur, Paris, FRANCE                                                     research.pasteur.fr  #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ============                                                                                               #
# = VERSIONS =                                                                                               #
# ============                                                                                               #
#                                                                                                            #
  VERSION=1.0.220308ac                                                                                       #
#                                                                                                            #
##############################################################################################################
  
##############################################################################################################
#                                                                                                            #
#  ================                                                                                          #
#  = INSTALLATION =                                                                                          #
#  ================                                                                                          #
#                                                                                                            #
#  Just give the execute permission to the script wgetENAHTS.sh with the following command line:             #
#                                                                                                            #
#   chmod +x YACO.sh                                                                                         #
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ==============================                                                                             #
# = STATIC FILES AND CONSTANTS =                                                                             #
# ==============================                                                                             #
#                                                                                                            #
# -- PWD: directory containing the current script ---------------------------------------------------------  #
#                                                                                                            #
  PWD="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null 2>&1 && pwd)";
#                                                                                                            # 
# -- constants --------------------------------------------------------------------------------------------  #
#                                                                                                            #
  NA="._N.o.N._.A.p.P.l.I.c.A.b.L.e_.";
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
#  ================                                                                                          #
#  = FUNCTIONS    =                                                                                          #
#  ================                                                                                          #
#                                                                                                            #
# = echoxit() ============================================================================================   #
#   prints in stderr the specified error message $1 and next exit 1                                          #
#                                                                                                            #
echoxit() {
  echo "$1" >&2 ; exit 1 ;
}    
#                                                                                                            #
# = randfile =============================================================================================   #
#   creates and returns a random file name that does not exist from the specified basename $1                #
#                                                                                                            #
randfile() {
  local rdf="$(mktemp $1.XXXXXXXXX)";
  echo $rdf ;
}
#                                                                                                            #
# = mandoc() =============================================================================================   #
#   prints the doc                                                                                           #
#                                                                                                            #
mandoc() {
  echo -e "\n\033[1m YACO v$VERSION                          $COPYRIGHT\033[0m";
  cat <<EOF

 USAGE:  YACO  -i <infile>  -r <reffile>  -o <basename>  [options]

 OPTIONS:
  -i <file>    FASTA file containing contigs to orient/order (mandatory)
  -r <file>    FASTA file containing the reference sequence (mandatory)
  -o <string>  basename for output files (mandatory)
  -w <int>     window (fragment) size (default: 400)
  -p <real>    minimum proportion of ranked fragments per contig (default: 0.5)
  -k <int>     blastn word size (default: 15)
  -c           contig cutting to fit circular reference genome (default: not set)
  -l <int>     wrap output sequences to the specified width (default: infinity)
  -t <int>     number of threads (default: 2)
  -v           verbose mode
  -h           prints this help and exits

EOF
}
#                                                                                                            #
##############################################################################################################

##############################################################################################################
#                                                                                                            #
# ================                                                                                           #
# = REQUIREMENTS =                                                                                           #
# ================                                                                                           #
#                                                                                                            #
# -- gawk -------------------------------------------------------------------------------------------------  #
#                                                                                                            #
  GAWK_BIN=gawk;
  [ ! $(command -v $GAWK_BIN) ] && echoxit "no $GAWK_BIN detected" ;
  GAWK_STATIC_OPTIONS="";     
  GAWK="$GAWK_BIN $GAWK_STATIC_OPTIONS";
  BAWK="$GAWK -F\" \"";
  TAWK="$GAWK -F\\t";
#                                                                                                            #
# -- NCBI BLAST+ (version >= 2.12.0) ----------------------------------------------------------------------  #
#                                                                                                            #
  export BLAST_USAGE_REPORT=false;         ## from https://www.ncbi.nlm.nih.gov/books/NBK569851/
#                                                                                                            #
# -- makeblastdb                                                                                             #
#                                                                                                            #
  MKBDB_BIN=makeblastdb;
  [ ! $(command -v $MKBDB_BIN) ] && echoxit "no $MKBDB_BIN detected" ;
  version="$($MKBDB_BIN -version | $GAWK '(NR==2){print$3}' | $GAWK -F"." '($1>=2&&$2>=12)')";
  [ -z "$version" ] && echoxit "incorrect version: $MKBDB_BIN" ;
  MKBDB_STATIC_OPTIONS="-input_type fasta";     
  MKBDB="$MKBDB_BIN $MKBDB_STATIC_OPTIONS";
  MKBNDB="$MKBDB -dbtype nucl";
#                                                                                                            #
# -- BLAST                                                                                                   #
#                                                                                                            #
  BLAST_STATIC_OPTIONS="-evalue 1e-5 -soft_masking false -max_target_seqs 1 -subject_besthit -max_hsps 1";
#                                                                                                            #
# -- blastn                                                                                                  #
#                                                                                                            #
  BLASTN_BIN=blastn;
  [ ! $(command -v $BLASTN_BIN) ] && echoxit "no $BLASTN_BIN detected" ;
  version="$($BLASTN_BIN -version | $GAWK '(NR==2){print$3}' | $GAWK -F"." '($1>=2&&$2>=12)')";
  [ -z "$version" ] && echoxit "incorrect version: $BLASTN_BIN" ;
  BLASTN_STATIC_OPTIONS="-task blastn -perc_identity 30 -dust no";
  BCOST="-reward 1 -penalty -1 -gapopen 5 -gapextend 2 -xdrop_ungap 20 -xdrop_gap 150 -xdrop_gap_final 100";
  BLASTN="$BLASTN_BIN $BLAST_STATIC_OPTIONS $BLASTN_STATIC_OPTIONS $BCOST";
#                                                                                                            #
##############################################################################################################


##############################################################################################################
####                                                                                                      ####
#### INITIALIZING PARAMETERS AND READING OPTIONS                                                          ####
####                                                                                                      ####
##############################################################################################################

if [ $# -lt 1 ]; then mandoc ; exit 1 ; fi

export LC_ALL=C ;

INFILE="$NA";        # -i
REFFILE="$NA";       # -r
BASENAME="$NA";      # -o
WSIZE=400;           # -w
FREQ=0.5;            # -p
KMER=11;             # -k
CCUT=false;          # -c
SLGT=0;              # -l 
NTHREADS=2;          # -t
VERBOSE=false;       # -v

while getopts i:r:o:w:p:k:l:t:cvh option
do
  case $option in
  i)  INFILE="$OPTARG"   ;;
  r)  REFFILE="$OPTARG"  ;;
  o)  BASENAME="$OPTARG" ;;
  w)  WSIZE=$OPTARG      ;;
  p)  FREQ=$OPTARG       ;;
  k)  KMER=$OPTARG       ;;
  c)  CCUT=true          ;;
  l)  SLGT=$OPTARG       ;;
  t)  NTHREADS=$OPTARG   ;;
  v)  VERBOSE=true       ;;
  h)  mandoc ;  exit 0   ;;
  :)  mandoc ;  exit 1   ;;
  \?) mandoc ;  exit 1   ;;
  esac
done

[ "$INFILE" == "$NA" ]           && echoxit "input file not specified (option -i)" ;
[ ! -e $INFILE ]                 && echoxit "file not found: $INFILE" ;
[   -d $INFILE ]                 && echoxit "not a file: $INFILE" ;
[ ! -s $INFILE ]                 && echoxit "empty file: $INFILE" ;
[ ! -r $INFILE ]                 && echoxit "no read permission: $INFILE" ;
[ "$REFFILE" == "$NA" ]          && echoxit "reference file not specified (option -r)" ;
[ ! -e $REFFILE ]                && echoxit "file not found: $REFFILE" ;
[   -d $REFFILE ]                && echoxit "not a file: $REFFILE" ;
[ ! -s $REFFILE ]                && echoxit "empty file: $REFFILE" ;
[ ! -r $REFFILE ]                && echoxit "no read permission: $REFFILE" ;
[ "$BASENAME" == "$NA" ]         && echoxit "basename not specified (option -o)" ;
[[ $WSIZE =~ ^[0-9]+$ ]]         || echoxit "incorrect value (option -w): $WSIZE" ; 
[ $WSIZE -lt 100 ]               && echoxit "window size should at least 100 (option -w)" ;
[ $WSIZE -gt 50000 ]             && echoxit "window size should at most 50000 (option -w)" ;
[[ $FREQ =~ ^0\.[0-9]+$ ]]       || echoxit "incorrect value (option -p): $FREQ" ;
[[ $KMER =~ ^[0-9]+$ ]]          || echoxit "incorrect value (option -k): $KMER" ; 
[ $KMER -lt 11 ]                 && echoxit "k-mer should at least 11 (option -k)" ;
[ $KMER -gt $(( $WSIZE / 2 )) ]  && echoxit "k-mer should at most half the window size (option -k)" ;
[[ $NTHREADS =~ ^[0-9]+$ ]]      || echoxit "incorrect value (option -t): $NTHREADS" ; 
[[ $SLGT =~ ^[0-9]+$ ]]          || echoxit "incorrect value (option -l): $SLGT" ;

n=$(grep -c "^>" $INFILE);
[ $n -eq 0 ]                     && echoxit "not a FASTA file: $INFILE" ;
[ $n -gt 9999 ]                  && echoxit "too many sequences ($n): $INFILE" ;
n=$(grep -c "^>" $REFFILE);
[ $n -eq 0 ]                     && echoxit "not a FASTA file: $REFFILE" ;

[ $SLGT -eq 0 ] && SLGT=9999999;

[ $NTHREADS -lt 1 ] && NTHREADS=1;
[ $NTHREADS -gt 1 ] && BLASTN="$BLASTN -num_threads $NTHREADS -mt_mode 1";

## tmp files #################################################################################################
FASTA=$(randfile $BASENAME.seq1);
FRAG1=$(randfile $BASENAME.frag1);
FRAG2=$(randfile $BASENAME.frag2);
OUT1=$(randfile $BASENAME.out1);
OUT2=$(randfile $BASENAME.out2);
TMP1=$(randfile $BASENAME.tmp1);
TMP2=$(randfile $BASENAME.tmp2);

## defining traps ############################################################################################
finalizer() {
  rm -f $FASTA $FASTA.ndb $FASTA.nhr $FASTA.nin $FASTA.not $FASTA.nsq $FASTA.ntf $FASTA.nto ;
  rm -f $FRAG1 $FRAG1.ndb $FRAG1.nhr $FRAG1.nin $FRAG1.not $FRAG1.nsq $FRAG1.ntf $FRAG1.nto ;
  rm -f $FRAG2 $FRAG2.ndb $FRAG2.nhr $FRAG2.nin $FRAG2.not $FRAG2.nsq $FRAG2.ntf $FRAG2.nto ;
  rm -f $OUT1 $OUT2 $TMP1 $TMP2 ;
}
echoxit() {
  finalizer ;
  echo -e "\n$1" >&2 ;
  exit 1 ;
}    
trap 'finalizer;exit 1'  INT ERR TERM ;                                      $VERBOSE && echo -n "[0%]." >&2 ;


##############################################################################################################
####                                                                                                      ####
#### PROCESSING REFFILE                                                                                   ####  
####                                                                                                      ####
##############################################################################################################

## rewriting REFERENCE #######################################################################################
#  + one sequence per line
$GAWK '!/^>/  {s=s$0;next}
       (s!=""){print s;s=""}
       END    {print s}' $REFFILE |               # one seq per line
  tr -d ' ' |                                     # deleting blank spaces and carriage returns
    tr '[a-z]' '[A-Z]' > $FASTA ;                 # uppercasing

## cutting REFERENCE into ws-long fragments ##################################################################
#  + >x  where  x = 2000000000 + frag coord
#  + >x  where  x = 3000000000 + frag number  (if other sequences next after the first)
#
#  position 1          ws/2       ws         3*ws/2     2*ws       5*ws/2     3*ws       7*ws/2     4*ws
#           |          |          |          |          |          |          |          |          |
#  ref      ACGTTGCCGGTGAACGTTGCCGGTGACGGTGAACGTTGCCGGTGTTGCCGGTGAACCGGTGAACGTTGCCGTGAACGTTGCCGGTGTTAGG...
#  frag1    [---------------------]
#  frag2               [---------------------]
#  frag3                          [---------------------]
#  frag4                                     [---------------------]
#  frag5                                                [---------------------]
#  frag6                                                           [---------------------]
#  frag7                                                                      [---------------------]
#  ...

{ hw=$(( $WSIZE / 2 ));
  head -1 $FASTA |                                # getting first sequence
    fold -w $hw |                                 # wrapping every WSIZE/2 nucleotides
      tr -d 'N?X-' |                              # deleting unknown character states
        $GAWK -v hw=$hw 'BEGIN  {x=2000000000}
                         (NR==1){fpre=$1;next}
                                {fcur=$1;
                                 frag=fpre""fcur;
                                 x+=hw;
                                 if(length(frag)>=hw){
                                   print">"x;
                                   print frag;
                                 }
                                 fpre=fcur;
                                }' ;
  sed 1d $FASTA |                                 # getting next sequence(s)
    tr -d '[:cntrl:]' |                           # discarding carriage returns
      fold -w $hw |                               # wrapping every WSIZE/2 nucleotides
        tr -d 'N?X-' |                            # deleting unknown character states
          $GAWK -v hw=$hw 'BEGIN  {x=3000000000}
                           (NR==1){fpre=$1;next}
                                  {fcur=$1;
                                   frag=fpre""fcur;
                                   ++x;
                                   if(length(frag)>=hw){
                                     print">"x;
                                     print frag;
                                   }
                                   fpre=fcur;
                                  }' ;               } > $FRAG2 ;                $VERBOSE && echo -n "." >&2 ;


##############################################################################################################
####                                                                                                      ####
#### PROCESSING INFILE                                                                                    ####  
####                                                                                                      ####
##############################################################################################################

## rewriting INFILE ##########################################################################################
#  + >x @@@@@"original header"    where  x = 10000 + sequence number
$GAWK 'BEGIN  {x=10000}
       !/^>/  {s=s$0;
               next;
              }
       (s!=""){if(index(s," ")!=0)gsub(s," ","");
               print s;
               s="";
              }
              {print">"(++x)" @@@@@"substr($0,2);}
       END    {if(index(s," ")!=0)gsub(s," ","");
               print s;
              }' $INFILE > $FASTA ;                                              $VERBOSE && echo -n "." >&2 ;


####################################################################################################
## circular genome contig cut                                                                     ##
####################################################################################################
if $CCUT
then
  ## formatting INFILE #############################################################################
  $MKBNDB -in $FASTA &>/dev/null ;

  ## getting first and last fragments from the first REFFILE sequence ##############################
  { head -2 $FRAG2 ;         ## first fragment (frag1)

    paste - - < $FRAG2 |     ## last fragment (frag2)
      grep -v "^>3" |
        tail -1 |
          $GAWK '{print$1;
                  print$2;}' ; } > $FRAG1 ;

  ## blastn searching of frag1 and frag2 againt INFILE #############################################
  ws="-word_size 11";
  #                                           output fields: 1----- 2--- 3----- 4--- 5----- 6--- 7----- 8--- 9-----
  $BLASTN -query $FRAG1 -db $FASTA -out $TMP1 $ws -outfmt '6 qseqid qlen qstart qend sseqid slen sstart send nident' 2>/dev/null ;

  rm -f $FASTA.ndb $FASTA.nhr $FASTA.nin $FASTA.not $FASTA.nsq $FASTA.ntf $FASTA.nto ;

  ## filtering out BLAST hits ######################################################################
  #  + nident/qlen < 0.3
  #  + (qend-qstart+1)/qlen < 0.35
  $TAWK '($1==q || ($4-$3+1)/$2<0.35){next} 
         {q=$1;print}' $TMP1 > $OUT1 ;

  ## searching cut point ###########################################################################
  #
  #                         ss1                        se1 ss2                        se2
  #                         |                            | |                            |
  #  fragments              >>>>>>>>>> frag1 >>>>>>>>>>>>> >>>>>>>>>> frag2 >>>>>>>>>>>>>
  #  contig to cut    ...ACGTTGCCGGTGAACGTTGCCGGTGACGGTGAACGTTGCCGGTGTTGCCGGTGAACCGGTGAACGTTGC...
  #  cut point                                            +
  #
  #  after sorting {ss1,se1,ss2,se2} into {p1,p2,p3,p4} such that p1 < p2 < p3 < p4, the cut point
  #  is at position between p2 and p3; however, p = p2 or p3 is selected when p = se1 or p = se2,
  #  as frag2 is sometimes incomplete (i.e. last fragment)
  #
  $TAWK '(NR==1){q1=$1;s1=$5;ss1=$7;se1=$8;next}
         (NR==2){q2=$1;s2=$5;ss2=$7;se2=$8;next}
         (NR>2) {exit}
         END    {if(s1!=s2)exit
                 p[1]=ss1;p[2]=se1;p[3]=ss2;p[4]=se2;
                 asort(p);
                 if(p[2]==ss1||p[2]==se1){print s1"\t"p[2];exit}
                 if(p[3]==ss1||p[3]==se1){print s1"\t"p[3];exit}
                }' $OUT1 > $TMP1 ;

  ## cutting contig and renumbering contig ids #####################################################
  if [ -s $TMP1 ]
  then
    c=$(cut -f1 $TMP1);   ## cutting contig c ...
    p=$(cut -f2 $TMP1);   ## ... at position p
    paste - - < $FASTA |
      tr -d '>' |
        $TAWK -v c=$c -v p=$p 'BEGIN{x=10000;}
                                    {id=substr($1,1,5);
                                     if(id==c){
                                       print">"(++x)substr($1,6)"::1-"p;
                                       print substr($2,1,p);
                                       print">"(++x)substr($1,6)"::"(++p)"-"length($2);
                                       print substr($2,p);
                                       next;
                                     }
                                     print">"(++x)substr($1,6);
                                     print$2;
                                    }' > $TMP2 ;
    mv $TMP2 $FASTA ;
    touch $TMP2 ;
  fi
fi

## cutting sequences into consecutive ws-long fragments ######################################################
#  + >(10000+x)(10000+y)  where  x = seq id  and  y = frag id in seq x
#  + x <= nseq < 9999
#  + y <= nfrag < 89999 per sequence, i.e. lgt < 89999*WSIZE
sed 's/ @@@@@.*//g' $FASTA |                        # keeping only the sequence number in the FASTA header
  tr '[a-z]' '[A-Z]' |                              # uppercasing
    fold -w $WSIZE |                                # wrapping every WSIZE nucleotides
      tr -d 'N?X-' |                                # deleting unknown character states
        $GAWK 'BEGIN{x=10000}
               /^>/ {++x;
                     y=10000;
                     next;
                    }
                    {if(length()<200)next;
                     print">"x""(++y);
                     print;
                    }' > $FRAG1 ;                                                $VERBOSE && echo -n "." >&2 ;


##############################################################################################################
####                                                                                                      ####
#### COMPARING GENOMES                                                                                    ####  
####                                                                                                      ####
##############################################################################################################

## formatting fragments ######################################################################################
$MKBNDB -in $FRAG1 &>/dev/null &  
$MKBNDB -in $FRAG2 &>/dev/null &

wait ;                                                                      $VERBOSE && echo -n ".[50%]" >&2 ;

## blastn similarity search between fragments ################################################################
BLASTN="$BLASTN -word_size $KMER";
#                                       output fields: 1----- 2--- 3----- 4--- 5----- 6--- 7----- 8--- 9-----
$BLASTN -query $FRAG1 -db $FRAG2 -out $TMP1 -outfmt '6 qseqid qlen qstart qend sseqid slen sstart send nident' 2>/dev/null ; $VERBOSE && echo -n "." >&2 ;
$BLASTN -query $FRAG2 -db $FRAG1 -out $TMP2 -outfmt '6 qseqid qlen qstart qend sseqid slen sstart send nident' 2>/dev/null ;

## filtering out BLAST hits ##################################################################################
#  + only fragments from the first REFERENCE sequence, i.e. id < 3000000000
#  + nident/qlen < 0.3
#  + nident/slen < 0.3
#  + (qend-qstart+1)/qlen < 0.35
#  + (|send-sstart|+1)/slen < 0.35
$TAWK 'function abs(x){return(x+=0)<0?-x:x}
       ($1==q || $5>3000000000 || $9/$2<0.3 || $9/$6<0.3 || ($4-$3+1)/$2<0.35 || (abs($8-$7)+1)/$6<0.35){next} 
       {q=$1;print}' $TMP1 > $OUT1 ;
$TAWK 'function abs(x){return(x+=0)<0?-x:x}
       ($1==q || $1>3000000000 || $9/$2<0.3 || $9/$6<0.3 || ($4-$3+1)/$2<0.35 || (abs($8-$7)+1)/$6<0.35){next} 
       {q=$1;print}' $TMP2 > $OUT2 ;

## assessing reciprocal BLAST hits (RBH) #####################################################################
#  sorting OUT2 according to the decreasing order of the subject ids (field 5)
sort -k5,5 -k9rg,9 $OUT2 > $TMP2 ; mv $TMP2 $OUT2 ; touch $TMP2 ;
# head $OUT1 ; head $OUT2 ; join -t$'\t' -1 1 -2 5 $OUT1 $OUT2 | head ;
# joining OUT1 and OUT2 to obtain RBHs (i.e. resulting fields $5 and $10 should be identical)
join -t$'\t' -1 1 -2 5 $OUT1 $OUT2 | $TAWK '($5==$10)' | cut -f1-9 > $TMP1 ;     $VERBOSE && echo -n "." >&2 ;

## counting fragments for each contig  #######################################################################
sed -n '1~2s/>//p' $FRAG1 |
  $GAWK '{curctg=substr($1,1,5);
          if(NR>1 && curctg!=ctg){
            print ctg"\t"n;
            n=0;
          }
          ++n;
          ctg=curctg;
         }
         END{print ctg"\t"n}' > $TMP2 ;

rm -f $FRAG1 $FRAG2 ;
rm -f $FRAG1.ndb $FRAG1.nhr $FRAG1.nin $FRAG1.not $FRAG1.nsq $FRAG1.ntf $FRAG1.nto ;
rm -f $FRAG2.ndb $FRAG2.nhr $FRAG2.nin $FRAG2.not $FRAG2.nsq $FRAG2.ntf $FRAG2.nto ;

## sorting contigs ###########################################################################################
#  + TMP1: RBH results between INFILE and REFFILE fragments
#  + TMP2: no. fragments per INFILE contig
#  + OUT1: contig id "\t" rank "\t" strand "\t" no. ranked feagment
$TAWK -v p=$FREQ '(FNR==NR){nfrg[$1]=$2;                                    # no. fragments per contig
                            next;
                           }
                           {curctg=substr($1,1,5);                          # current contig id
                            if(FNR>1 && curctg!=ctg){                       # the current contig is a new one
                              if(n>=p*nfrg[ctg]){                           # at least p*nfrg fragments should match
                                asort(coord);
                                rank=coord[1+int(n/2)];                     # rank = median coordinate
                                strand=((fwd>rev)?"+":"-");
                                print ctg"\t"rank"\t"strand"\t"n"\t"nfrg[ctg];
                              }
                              delete coord;
                              n=fwd=rev=0;
                            }
                            ctg=curctg;
                            coord[(++n)]=$5-2000000000;                     # hit coordinate within the ref
                            ($8-$7>0)&&(++fwd)||(++rev);                    # same or different directions
                           }
                  END      {if(n>=p*nfrg[ctg]){                             # at least p*nfrg fragments should match
                              asort(coord);
                              rank=coord[1+int(n/2)];
                              strand=((fwd>rev)?"+":"-");
                              print ctg"\t"rank"\t"strand"\t"n"\t"nfrg[ctg];
                            }}' $TMP2 $TMP1 |
  sort -k2n,2 > $OUT1 ;                                                          $VERBOSE && echo -n "." >&2 ;


##############################################################################################################
####                                                                                                      ####
#### WRITING OUTFILES                                                                                     ####  
####                                                                                                      ####
##############################################################################################################

## info.txt ##################################################################################################
$GAWK 'BEGIN   {print"#ordered/oriented sequences"}
       /^>/    {id=substr($0,2,5);
                line=substr($0,13);
                fh[id]=line;
                next;
               }
       (FNR<NR){if($3=="+")print fh[$1]"\t+\t"$4"/"$5;
                else       print fh[$1]"\t-\t"$4"/"$5;
                delete fh[$1];
               }
       END     {print"#unlocalized sequences";
                for(id in fh)print fh[id];
               }' $FASTA $OUT1 > $BASENAME.info.txt ;                            $VERBOSE && echo -n "." >&2 ;

## reo.fasta #################################################################################################
#  ordering sequences
$GAWK '/^>/     {id=substr($0,2,5);
                 line=substr($0,13);
                 fh[id]=line;
                 next;
                }
       (FNR==NR){seq[id]=$0}
       (FNR<NR) {if($3=="+")print fh[$1]"\t+\t"seq[$1];
                 else       print fh[$1]"\t-\t"seq[$1];
                }' $FASTA $OUT1 > $TMP1 ;
#  writing FASTA and reverse-completing (if required)
while IFS=$'\t' read -r fh strand seq
do
  if [ "$strand" == "+" ]
  then
    echo ">$fh +" ; 
    fold -w $SLGT <<<"$seq" ;
  else
    echo ">$fh -" ; 
    rev <<<"$seq" |
      tr 'ACGTRYSWKMBDHVNX' 'TGCAYRSWMKVHDBNX' |
        fold -w $SLGT ;
  fi
done < $TMP1 > $BASENAME.reo.fasta ;                                             $VERBOSE && echo -n "." >&2 ;

## unc.fasta #################################################################################################
$GAWK -v l=$SLGT '/^>/     {id=substr($0,2,5);
                            line=substr($0,13);
                            fh[id]=line;
                            next;
                           }
                  (FNR==NR){seq[id]=$0}
                  (FNR<NR) {delete fh[$1]}
                  END      {for(id in fh){
                              print">"fh[id];
                              s=seq[id];
                              while (length(s)>l) {
                                print substr(s,1,l);
                                s=substr(s,l+1);
                              }
                              if(length(s)>0)print s;
                            }
                           }' $FASTA $OUT1 > $BASENAME.unl.fasta ;


##############################################################################################################
####                                                                                                      ####
#### EXITING                                                                                              ####  
####                                                                                                      ####
##############################################################################################################

rm -f $OUT1  $TMP1 ;
rm -f $OUT2  $TMP2 ;
rm -f $FASTA ;                                                                 $VERBOSE && echo "[100%]" >&2 ;

$VERBOSE && echo "contig sequences          $INFILE" ;
$VERBOSE && echo "reference sequence        $REFFILE" ;
$VERBOSE && echo "ordered/oriented contigs  $BASENAME.reo.fasta" ;
$VERBOSE && echo "unlocalized contigs       $BASENAME.unl.fasta" ;
$VERBOSE && echo "tab-delimited summary     $BASENAME.info.txt" ;
    
exit ;
































## info.txt ##################################################################################################
# echo "#classified sequences" > $BASENAME.info.txt ;
# while IFS=$'\t' read -r id _ strand
# do
#   grep -m1 -A1 "^>$id @@@@@" $FASTA > $TMP1 ;
#   fh=$(head -1 $TMP1 | sed 's/>.*@@@@@/>/');
#   if [ "$strand" == "+" ]
#   then
#     echo -e "${fh:2}\t+" ;
#   else 
#     echo -e "${fh:2}\t-" ;
#   fi
# done < $OUT1 >> $BASENAME.info.txt ;
# echo "#unclassified sequences" >> $BASENAME.info.txt ;
# grep "^>" $FASTA |
#   tr -d '>' |
#     while read -r idfh
#     do
#       id=$(sed 's/ @@@@@.*//' <<<"$idfh");
#       if [ $(grep -m1 -c "^$id" $OUT1) -ne 0 ]; then continue ; fi 
#       sed 's/^.*@@@@@/>/' <<<"$idfh" ;
#     done >> $BASENAME.info.txt ;
## reo.fasta #################################################################################################
# while IFS=$'\t' read -r id _ strand
# do
#   grep -m1 -A1 "^>$id @@@@@" $FASTA > $TMP1 ;
#   fh=$(head -1 $TMP1 | sed 's/>.*@@@@@/>/');
#   if [ "$strand" == "+" ]
#   then
#     echo "$fh +" ; 
#     sed 1d $TMP1 |
#       fold -w $SLGT ;
#   else
#     echo "$fh -" ; 
#     sed 1d $TMP1 |
#       rev |
#         tr 'ACGTRYSWKMBDHVNX' 'TGCAYRSWMKVHDBNX' |
#           fold -w $SLGT ;
#   fi
# done < $OUT1 > $BASENAME.reo.fasta ;
## unc.fasta #################################################################################################
# paste - - < $FASTA |
#   while IFS=$'\t' read -r idfh seq
#   do
#     id=$(sed 's/ @@@@@.*//' <<<"$idfh" | tr -d '>');
#     if [ $(grep -m1 -c "^$id" $OUT1) -ne 0 ]; then continue; fi
#     sed 's/>.* @@@@@/>/' <<<"$idfh" ;
#     fold -w $SLGT <<<"$seq";
#   done > $BASENAME.unc.fasta ;
